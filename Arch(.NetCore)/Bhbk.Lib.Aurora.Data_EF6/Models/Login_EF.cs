//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bhbk.Lib.Aurora.Data_EF6.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Login_EF
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Login_EF()
        {
            this.Alerts = new HashSet<Alert_EF>();
            this.FileSystems = new HashSet<FileSystemLogin_EF>();
            this.Networks = new HashSet<Network_EF>();
            this.PrivateKeys = new HashSet<PrivateKey_EF>();
            this.PublicKeys = new HashSet<PublicKey_EF>();
            this.Settings = new HashSet<Setting_EF>();
            this.Sessions = new HashSet<Session_EF>();
        }
    
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public int AuthTypeId { get; set; }
        public bool IsPasswordRequired { get; set; }
        public bool IsPublicKeyRequired { get; set; }
        public int DebugTypeId { get; set; }
        public string EncryptedPass { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeletable { get; set; }
        public string Comment { get; set; }
        public System.DateTimeOffset CreatedUtc { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Alert_EF> Alerts { get; set; }
        public virtual LoginAuthType_EF AuthType { get; set; }
        public virtual LoginDebugType_EF DebugType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FileSystemLogin_EF> FileSystems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Network_EF> Networks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrivateKey_EF> PrivateKeys { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PublicKey_EF> PublicKeys { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Setting_EF> Settings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session_EF> Sessions { get; set; }
        public virtual LoginUsage_EF Usage { get; set; }
    }
}
