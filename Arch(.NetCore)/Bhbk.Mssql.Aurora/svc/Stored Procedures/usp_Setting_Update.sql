﻿CREATE PROCEDURE [svc].[usp_Setting_Update] @Id UNIQUEIDENTIFIER,
	@ConfigKey NVARCHAR(MAX),
	@ConfigValue NVARCHAR(MAX),
	@IsDeletable BIT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION;

		UPDATE [dbo].[tbl_Setting]
		SET ConfigKey = @ConfigKey,
			ConfigValue = @ConfigValue,
			IsDeletable = @IsDeletable
		WHERE Id = @Id

		IF @@ROWCOUNT != 1 THROW 51000,
			'ERROR',
			1;
			SELECT *
			FROM [dbo].[tbl_Setting]
			WHERE Id = @Id

		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		THROW;
	END CATCH
END
