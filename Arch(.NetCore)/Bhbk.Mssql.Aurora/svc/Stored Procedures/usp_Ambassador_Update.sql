﻿CREATE PROCEDURE [svc].[usp_Ambassador_Update] @Id UNIQUEIDENTIFIER,
	@UserPrincipalName NVARCHAR(128),
	@EncryptedPass NVARCHAR(128),
	@IsEnabled BIT,
	@IsDeletable BIT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION;

		UPDATE [dbo].[tbl_Ambassador]
		SET UserPrincipalName = @UserPrincipalName,
			EncryptedPass = @EncryptedPass,
			IsEnabled = @IsEnabled,
			IsDeletable = @IsDeletable
		WHERE Id = @Id

		IF @@ROWCOUNT != 1 THROW 51000,
			'ERROR',
			1;
			SELECT *
			FROM [dbo].[tbl_Ambassador]
			WHERE Id = @Id

		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		THROW;
	END CATCH
END
