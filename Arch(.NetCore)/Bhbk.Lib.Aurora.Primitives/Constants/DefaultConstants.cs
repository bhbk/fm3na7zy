﻿using System;

namespace Bhbk.Lib.Aurora.Primitives
{
    public class DefaultConstants
    {
        public const string RoleForAdmins_Aurora = "Aurora.Admins";
        public const string RoleForUsers_Aurora = "Aurora.Users";
        public const string RoleForViewers_Aurora = "Aurora.Viewers";
        public const string RoleForDaemonUsers_Aurora = "Aurora.DaemonUsers";
    }
}
