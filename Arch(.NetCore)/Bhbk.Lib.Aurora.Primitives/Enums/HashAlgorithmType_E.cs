﻿using System;

namespace Bhbk.Lib.Aurora.Primitives.Enums
{
    public enum HashAlgorithmType_E
    {
        None = 0,
        MD2 = 10,
        MD4 = 11,
        MD5 = 12,
        SHA1 = 20,
        SHA256 = 21,
        SHA384 = 22,
        SHA512 = 23,
    }
}
