﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bhbk.Lib.Aurora.Primitives.Enums
{
    public enum AuthFactorType_E
    {
        PasswordOnly = 1,
        PublicKeyOnly = 2,
        PasswordAndPublicKey = 3,
    }
}
