﻿using System;

namespace Bhbk.Lib.Aurora.Primitives.Enums
{
    public enum AuthType_E
    {
        Identity = 1,
        Local = 2,
    }
}
