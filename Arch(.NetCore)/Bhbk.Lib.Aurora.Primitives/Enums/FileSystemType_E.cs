﻿using System;

namespace Bhbk.Lib.Aurora.Primitives.Enums
{
    public enum FileSystemType_E
    {
        Database = 1,
        Memory = 2,
        SMB = 3,
    }
}
