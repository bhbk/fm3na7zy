﻿using System;

namespace Bhbk.Lib.Aurora.Primitives.Enums
{
    public enum NetworkActionType_E
    {
        Allow = 1,
        Deny = 2,
    }
}
